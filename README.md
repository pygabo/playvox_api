playvox-api
===========

playvox test

License
:   MIT

| Python        | Django        | MongoDB     |
| ------------- |:-------------:| -----------:| 
|    3.7        |  3.0.9        | 4.4 latest  |


Settings
--------

Moved to
[settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

Basic Commands
--------------

### How to run services with Docker and Docker Compose 

    docker-compose build
    
    docker-compose up -d
    
    docker-compose run --rm django python manage.py migrate
    
## How to find api document with postman

    /playvox_api/postman/collection.json
    
    /playvox_api/postman/environment.json
       
### Registration

    curl --location --request POST '0.0.0.0:8000/rest-auth/registration/' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "username": "ricksanchezc",
        "email": "ricksanchez@gmail.com",
        "password1": "UniversoC137",
        "password2": "UniversoC137"
    }''

### Login 

    curl --location --request POST '0.0.0.0:8000/auth-token/' \
    --header 'Content-Type: application/json' \
    --data-raw '{
	    "username": "ricksanchezc",
	    "password": "UniversoC137"
    }'


### How to load Daily Weather Workbook

    curl --location --request POST '0.0.0.0:8000/api/workbook/' \
    --header 'Content-Type: application/json' \
    --header 'Authorization: Token acdd5b67b5b0deabeabf43c62c09d0d491a51583' \
    --form 'document=@/Users/pygabo/Downloads/daily_weather.xlsx' \
    --form 'start_date=2011-09-01' \
    --form 'data_set=21'

### How to create Daily Weather Dataset

    curl --location --request GET '0.0.0.0:8000/api/dataset/21/?start_date=2011-09-01' \
    --header 'Content-Type: application/json' \
    --header 'Authorization: Token acdd5b67b5b0deabeabf43c62c09d0d491a51583' \
    --data-raw '{
        "data_set_name": " Universo C137"
    }'

### Setting Up Your Users

-   To create an **superuser account**, use this command:

        $ docker-compose run --rm django python manage.py createsuperuser

### Type checks

Running type checks with mypy:

    $ mypy playvox_api

### Test coverage

To run the tests, check your test coverage, and generate an HTML
coverage report:

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

#### Running tests with py.test

    $ pytest

### Celery

This app comes with Celery.

To run a celery worker:

``` {.sourceCode .bash}

cd playvox_api
celery -A config.celery_app worker -l info

```

Please note: For Celery's import magic to work, it is important *where*
the celery commands are run. If you are in the same folder with
*manage.py*, you should be right.

Deployment
----------

The following details how to deploy this application.

### Docker

See detailed [cookiecutter-django Docker
documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).

## GitLab CI/CD 
![img](https://i.imgur.com/UwWFjJz.png)
## mongoDB
![img](https://i.imgur.com/f165emd.png)

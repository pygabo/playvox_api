from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):
    def has_object_permission(self, request, view, obj):
        try:
            return obj.created_by.id == request.user.id
        except Exception:
            return False

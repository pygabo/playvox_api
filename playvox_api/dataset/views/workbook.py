from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated

from playvox_api.dataset.serializers.workbook import WorkbookSerializer
from playvox_api.dataset.tasks import populate
from playvox_api.utils.permission import IsOwner


class WorkbookListCreate(CreateAPIView):
    serializer_class = WorkbookSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def perform_create(self, serializer):
        instance = serializer.save()
        # instantiate celery Tasks delay  # This may take a while
        populate.delay(workbook_file_id=instance.id)

from datetime import datetime

from django.conf import settings
from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from playvox_api.dataset.models import DataSet
from playvox_api.dataset.serializers.dataset import (
    CreateDataSetSerializer,
    DataSetSerializer,
)
from playvox_api.utils.permission import IsOwner


class DataSetListCreate(ListCreateAPIView):
    serializer_class = CreateDataSetSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self, *args, **kwargs):
        return DataSet.objects.filter(
            created_by=self.request.user
        ).prefetch_related('book')

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class DataSetDetail(RetrieveAPIView):
    serializer_class = DataSetSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self, *args, **kwargs):
        return DataSet.objects.filter(
            created_by=self.request.user
        ).prefetch_related('book')

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data

        data['mongo_data'] = self.get_data_set(
            start_date=request.GET.get('start_date'),
            end_date=request.GET.get('end_date'),
            set_id=instance.id,
        )
        return Response(data)

    def get_data_set(self, start_date, end_date, set_id):
        database = settings.MONGO_CLIENT['daily_weather_data_sets']
        collection = 'collection_set_weather'

        mongo_query = {
            'set_id': set_id,
        }

        if start_date or end_date:
            mongo_query['date'] = {}
            if start_date:
                start_date = datetime.combine(datetime.strptime(start_date, '%Y-%m-%d').date(), datetime.min.time())
                mongo_query['date']['$gte'] = start_date
            if end_date:
                end_date = datetime.combine(datetime.strptime(end_date, '%Y-%m-%d').date(), datetime.max.time())
                mongo_query['date']['$lt'] = end_date

        collection = database[collection]
        mongo_data = self.parse_mongo_to_json(collection.find(mongo_query))

        return {
            'mongo_data': mongo_data,
        }

    @staticmethod
    def parse_mongo_to_json(mongo_data):
        data = []
        for i in mongo_data:
            del i['_id']
            data.append(i)
        return data

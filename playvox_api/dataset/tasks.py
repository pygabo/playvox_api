from datetime import datetime, timedelta

import xlrd
from django.conf import settings
from django.shortcuts import get_object_or_404

from config import celery_app
from playvox_api.dataset.models import Workbook


@celery_app.task()
def populate(workbook_file_id):
    get_object_workbook = get_object_or_404(Workbook, id=workbook_file_id)

    object_workbook = xlrd.open_workbook(file_contents=get_object_workbook.document.read(), encoding_override='utf8')
    xl_sheet = object_workbook.sheet_by_index(0)
    sheet_names = object_workbook.sheet_names()
    num_cols = xl_sheet.ncols  # Number of columns
    documents = []
    date = datetime.combine(get_object_workbook.start_date, datetime.min.time())
    for row_idx in range(1, xl_sheet.nrows):  # Iterate through rows
        # ('Row: %s' % row_idx)  # Print row number
        data = {
            'set_id': get_object_workbook.data_set.id,
            'id_workbook': get_object_workbook.id,
            'workbook_name': sheet_names[0],
            'date': date,
        }
        date += timedelta(days=1)  # Add date next
        for col_idx in range(0, num_cols):  # Iterate through columns
            cell_obj = xl_sheet.cell(row_idx, col_idx)  # Get cell object by row, col
            data[str(xl_sheet.cell(0, col_idx).value)] = cell_obj.value
            # 'Column: [%s] cell_obj: [%s]' % (col_idx, cell_obj)

        documents.append(data)

    # Mongo Client
    database = settings.MONGO_CLIENT['daily_weather_data_sets']
    collection = 'collection_set_weather'
    set_collection = database[collection]
    set_collection.insert_many(documents)

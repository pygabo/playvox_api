from django.apps import AppConfig


class DatasetConfig(AppConfig):
    name = 'playvox_api.dataset'

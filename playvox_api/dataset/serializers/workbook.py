from rest_framework.serializers import DateField, ModelSerializer

from playvox_api.dataset.models import Workbook


class WorkbookSerializer(ModelSerializer):
    start_date = DateField(format="%Y-%m-%d", required=True)

    class Meta:
        model = Workbook
        fields = (
            'id',
            'document',
            'data_set',
            'created',
            'start_date'
        )
        read_only_fields = (
            'id',
        )

from rest_framework.serializers import ModelSerializer

from playvox_api.dataset.models import DataSet
from playvox_api.dataset.serializers.workbook import WorkbookSerializer


class CreateDataSetSerializer(ModelSerializer):
    class Meta:
        model = DataSet
        fields = (
            'id',
            'data_set_name',
            'created_by',
        )
        read_only_fields = (
            'id',
            'created_by',
        )


class DataSetSerializer(ModelSerializer):
    book = WorkbookSerializer(many=True)

    class Meta:
        model = DataSet
        fields = (
            'id',
            'data_set_name',
            'created_by',
            'book',
            'created',
        )
        read_only_fields = (
            'id',
            'document',
            'created_by',
            'book',
        )

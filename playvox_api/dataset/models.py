from django.conf import settings
from django.db.models import PROTECT, CharField, DateField, FileField, ForeignKey
from django.utils import timezone
from model_utils.models import TimeStampedModel


class DataSet(TimeStampedModel):
    data_set_name = CharField(max_length=255)  # Name Daily Weather Dataset
    created_by = ForeignKey(settings.AUTH_USER_MODEL, on_delete=PROTECT, null=True, blank=True)  # user owner

    class Meta:
        unique_together = ('data_set_name', 'created_by')


class Workbook(TimeStampedModel):
    document = FileField()  # Document File
    data_set = ForeignKey(DataSet, on_delete=PROTECT, related_name='book')  # Daily Weather Dataset
    start_date = DateField(default=timezone.now)  # Start Date

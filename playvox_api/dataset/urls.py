# Base imports
from django.urls import path

from playvox_api.dataset.views.dataset import DataSetDetail, DataSetListCreate
from playvox_api.dataset.views.workbook import WorkbookListCreate

app_name = 'dataset'

urlpatterns = [
    path('dataset/', DataSetListCreate.as_view(), name='dataset_list_create'),
    path('dataset/<int:pk>/', DataSetDetail.as_view()),
    path('workbook/', WorkbookListCreate.as_view(), name='workbook_list'),
]
